# Recipe Aws Api

NGINX proxy for app API

## Usage 

### Environment Variables

 * `LISTEN_PORT` - port to listen on (default: `8000`)
 * `APP_HOST` - app hostname to forward requests to (default: `app`)
 * `APP_PORT` - app port to to forward requests to (default: `9000`)
 